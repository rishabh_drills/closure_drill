function cacheFunction(cb)
{
    let cache = [];
    function callBackFunc(n)
    {
        // let idx = n.toString();
        if(cache[n] === undefined)
        {
            cache[n] = cb(n);
            return cache[n];
        }else{
            let cacheResult = cb(n);
            cache[n] = cacheResult;
            return cacheResult;
        }
        
    }
    return callBackFunc;
    
}

module.exports = cacheFunction;