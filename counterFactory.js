function counterFactory(value)
{
    let currValue = value;
    let counterObject = {
        increment: function(){
            currValue += 1;
            return currValue;
        },
         decrement: function(){
            currValue -= 1;
            return currValue;
        }
    }
    return counterObject;
    
}

module.exports = counterFactory;