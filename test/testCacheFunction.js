const cacheFunction = require('../cacheFunction');

function cb(a){
    return a + a;
}
let test = cacheFunction(cb);

console.log(test(5));
console.log(test(2));
console.log(test(3));

// test(1);
// test(3);
// test(2);
