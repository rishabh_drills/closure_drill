function limitFunctionCallCount(callback, n)
{
    let limitedCall = 0;
    function counter(elements)
    {
        limitedCall = limitedCall + 1;
        if(limitedCall <= n)
        {
            return callback(elements);
        }else{
            return null;
        }
    }
    return counter;
}


module.exports = limitFunctionCallCount;







// function limitFunctionCallCount(cb, n = 1)
// {
//     let count = n;
//     function callBackFunc(...arg)
//     {
//         if(count > 0)
//         {
//             count--;
            
//             return cb(...arg)
//         }
//         else{
//             return null;
//         }
//     }
//     return callBackFunc;
    
// }

