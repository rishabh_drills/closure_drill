const counterFactory = require('../counterFactory');

let counter = counterFactory(20);
console.log(counter.increment());
console.log(counter.decrement());